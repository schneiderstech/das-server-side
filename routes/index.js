var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next){
    res.render('../../das-client-side/index.html', { title: 'My App' });
});

module.exports = router;