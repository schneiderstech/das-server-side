var express = require('express');
var router  = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

var ProcessController = require('./../controllers/process');

router.get('/listallprocesses', ProcessController.listAllProcesses);
router.post('/addnewprocess', ProcessController.addProcess);
router.put('/deleteprocess', ProcessController.deleteProcess);

module.exports = router;