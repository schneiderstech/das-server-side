var ProcessModel = require('../models/process');

// list all processes
module.exports.listAllProcesses = function(req, res) {
    ProcessModel
    .find().sort({_id: -1}).find({}, function(err, listallprocesses){
        if(err){
          console.log(err);
        } else {
            res.json(listallprocesses);
        }
    });
};

// add process
module.exports.addProcess = function(req, res) {
    var randomData = [];
    var randomDates = [];

    while(randomData.length < 10){
        var randomNumber = Math.ceil(Math.random()*100)
        if(randomData.indexOf(randomNumber) > -1) continue;
            randomData[randomData.length] = randomNumber;
    };

    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    while(randomDates.length < 3){
        randomDates.push(randomDate(new Date(2012, 0, 1), new Date()));
    };


    var process = new ProcessModel({
        das_percentage: req.body.das_percentage,
        das_cpu_data: randomData,
        das_status: req.body.das_status,
        das_instances: req.body.das_instances,
        das_memory: req.body.das_memory,
        das_restarts: randomDates
    })

    process.save(function(err, done) {
        if(err) {
            console.log(err);
            return next(err);
        } else {
            console.log(done);
            res.status(201).json(done);            
        }
    });

    function randomArray(length, max) {
        return Array.apply(null, Array(length)).map(function() {
            return Math.round(Math.random() * max);
        });
    }

};

// delete process
module.exports.deleteProcess = function(req, res, next) {
    console.log('id is:', req.body.id);
    ProcessModel
    .remove( { _id : req.body.id}, function(err, done){
        if(err) 
            return res.json(err);
        return res.json(done);
    });
};


