# README #

This is the Back End App framework using NodeJS.


### Setup NodeJS Framework Dependencies ###

* Create a folder called: **dashboard-monitor**
* Clone **das-server-side** repository inside the folder **dashboard-monitor** 
* Run **npm install**: 
This will install all modules required for the Front End Framework
* Setup your MongoDB Database connection. For a complete walk-through to install MongoDB, please visit: [NPM Install MongoDB](https://www.npmjs.com/package/mongodb) 

For more information, how to install NodeJS, please visit:
[How to install NodeJS](https://nodejs.org/en/)


### Start the app ###
To start the app, please run: 
```node server.js```


### MongoDB Collection ###
* To manage your MongoDB Collections use the IDE Studio 3T, to find out more, visit: [IDE Studio 3T](https://studio3t.com)


### What's next? ###
After done the Back End setup, follow the Front End Framework setup [in this link](https://bitbucket.org/schneiderstech/das-client-side#readme).