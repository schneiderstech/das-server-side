var mongoose = require('mongoose');
 
var ProcessSchema = new mongoose.Schema({
    das_percentage: {
        type: Number,
        required: false
    },
    das_cpu_data: [Number],
    das_status: {
        type: String
    },
    das_instances: {
        type: Number
    },
    das_memory: {
        type: Number
    },
    das_restarts: [Date]
});

mongoose = require('mongoose').set('debug', true);


module.exports = mongoose.model('vizexplorer', ProcessSchema);
