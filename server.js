var express     = require('express');
var path        =  require('path');
var bodyParser  = require('body-parser');

var databaseConfig = require('./config/database');
var mongoose    = require('mongoose');

var index       = require('./routes/index');
var process     = require('./routes/process');

var port = 8080;
var app = express();


// CONNECTION EVENTS
mongoose.connect(databaseConfig.url);

// When successfully connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connection open to ' + databaseConfig.url);
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Set static folder
// add static files from client-side directory
app.use(express.static(path.join(__dirname, '..', '/das-client-side/src')));
app.use(express.static(path.join(__dirname, '..', '/das-client-side/node_modules')));

// Body Parser 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/', index);
app.use('/api', process);

app.listen(port, function() {
    console.log('Server started on port ' + port);
});
